﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("boinst-utils-full")]
[assembly: AssemblyDescription("This assembly contains generally useful utility classes that require the Full .NET Profile (as opposed to the Client Profile).")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Boinst")]
[assembly: AssemblyProduct("Boinst.Utils")]
[assembly: AssemblyCopyright("Copyright ©  2012 Boinst")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("593d53aa-960d-4128-b674-d868476bff6f")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
