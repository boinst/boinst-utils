namespace Boinst.Utils.Tests.Reporting
{
    using Boinst.Utils.Reporting;

    using NUnit.Framework;

    [TestFixture]
    public class PhasedTaskTests
    {
        [Test]
        public void OnePhase()
        {
            var pc = new PhasedTask();
            var p1 = new ProgressPhase();
            pc.Add(p1);
            p1.Progress = 0.25;
            Assert.AreEqual(pc.Progress, 0.25, 1e-6);
            p1.Progress = 0.50;
            Assert.AreEqual(pc.Progress, 0.50, 1e-6);
            p1.Progress = 0.75;
            Assert.AreEqual(pc.Progress, 0.75, 1e-6);
        }

        [Test]
        public void TwoPhases()
        {
            var pc = new PhasedTask();
            var p1 = new ProgressPhase();
            pc.Add(p1);
            var p2 = new ProgressPhase();
            pc.Add(p2);
            p1.Progress = 0.5;
            Assert.AreEqual(pc.Progress, 0.25, 1e-6);
            p1.Progress = 1.0;
            Assert.AreEqual(pc.Progress, 0.50, 1e-6);
            p2.Progress = 0.0;
            Assert.AreEqual(pc.Progress, 0.50, 1e-6);
            p2.Progress = 0.5;
            Assert.AreEqual(pc.Progress, 0.75, 1e-6);
            p2.Progress = 1.0;
            Assert.AreEqual(pc.Progress, 1.0, 1e-6);
        }

        [Test]
        public void WeightedPhases()
        {
            var pc = new PhasedTask();
            var p1 = new ProgressPhase();
            pc.Add(p1);
            var p2 = new ProgressPhase(3.0);
            pc.Add(p2);
            p1.Progress = 0.5;
            Assert.AreEqual(pc.Progress, 0.125, 1e-6);
            p1.Progress = 1.0;
            Assert.AreEqual(pc.Progress, 0.25, 1e-6);
            p2.Progress = 1.0 / 3.0;
            Assert.AreEqual(pc.Progress, 0.50, 1e-6);
            p2.Progress = 2.0 / 3.0;
            Assert.AreEqual(pc.Progress, 0.75, 1e-6);
            p2.Progress = 1.0;
            Assert.AreEqual(pc.Progress, 1.0, 1e-6);
        }

        [Test]
        public void IterativePhaseTest()
        {
            var pc = new PhasedTask();
            var p1 = new ProgressPhase();
            pc.Add(p1);
            var p2 = new MultipleIterationPhase(3); // A Phase with 3 iterations
            pc.Add(p2);
            p1.Progress = 0.5; // Phase 1 is 50% complete
            Assert.AreEqual(pc.Progress, 0.25, 1e-6); // Overall progress is 25%
            p1.Progress = 1.0; // Phase 1 is complete
            Assert.AreEqual(pc.Progress, 0.5, 1e-6); // Overall progress is 50%
            p2.UpdateProgress(2, 0.5); // Phase 2 is 50% through the second of 3 iterations
            Assert.AreEqual(pc.Progress, 0.75, 1e-6); // Overall progress is 75%
        }
    }
}