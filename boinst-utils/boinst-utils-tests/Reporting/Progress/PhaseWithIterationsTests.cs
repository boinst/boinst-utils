namespace Boinst.Utils.Tests.Reporting
{
    using Boinst.Utils.Reporting;

    using NUnit.Framework;

    /// <summary>
    /// Tests for the <see cref="MultipleIterationPhase"/> class.
    /// </summary>
    [TestFixture]
    public class PhaseWithIterationsTests
    {
        [Test]
        public void Test()
        {
            MultipleIterationPhase phase = new MultipleIterationPhase(1);
            phase.UpdateProgress(1, 0.5);
            Assert.AreEqual(0.5, phase.Progress, 1e-6);
        }

        [Test]
        public void TestHalfwayThrough1Of2()
        {
            MultipleIterationPhase phase = new MultipleIterationPhase(2);
            phase.UpdateProgress(1, 0.5);
            Assert.AreEqual(0.25, phase.Progress, 1e-6);
        }

        [Test]
        public void TestHalfwayThrough2Of2()
        {
            MultipleIterationPhase phase = new MultipleIterationPhase(2);
            phase.UpdateProgress(2, 0.5);
            Assert.AreEqual(0.75, phase.Progress, 1e-6);
        }

        [Test]
        public void TestStarted2Of2()
        {
            MultipleIterationPhase phase = new MultipleIterationPhase(2);
            phase.UpdateProgress(2, 0.0);
            Assert.AreEqual(0.5, phase.Progress, 1e-6);
        }
    }
}