namespace Boinst.Utils.Tests.Reporting
{
    using Boinst.Utils.Reporting;

    using NUnit.Framework;

    [TestFixture]
    public class PhaseTests
    {
        [Test]
        public void EventTriggered()
        {
            bool triggered = false;
            var phase = new ProgressPhase();
            phase.ProgressChanged += delegate { triggered = true; };
            phase.Progress = 0.5;

            Assert.IsTrue(triggered);
        }
    }
}