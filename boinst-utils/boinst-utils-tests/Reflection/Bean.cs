namespace Boinst.Utils.Tests.Reflection
{
    using System;
    using System.ComponentModel;
    using System.Linq.Expressions;

    using Boinst.Utils.Reflection;

    /// <summary>
    /// A fictional test class. It's a Bean.
    /// </summary>
    internal class Bean : INotifyPropertyChanged
    {
        /// <summary>
        /// The number of shoots on the bean.
        /// </summary>
        private int shoots;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The number of shoots on the bean.
        /// </summary>
        public int Shoots
        {
            get
            {
                return this.shoots;
            }

            set
            {
                this.shoots = value;
                this.OnPropertyChanged(b => b.Shoots);
            }
        }

        /// <summary>
        /// Trigger the <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <typeparam name="T">
        /// The type for the expression.
        /// </typeparam>
        /// <param name="expression">
        /// The expression.
        /// </param>
        private void OnPropertyChanged<T>(Expression<Func<Bean, T>> expression)
        {
            var handler = this.PropertyChanged;
            if (handler == null) return;
            var propertyName = PropertyInfoHelper<Bean>.GetPropertyInfo(expression).Name;
            handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}