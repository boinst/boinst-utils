﻿namespace Boinst.Utils.Tests.Reflection
{
    using Boinst.Utils.Reflection;

    using NUnit.Framework;

    /// <summary>
    /// Tests for the <see cref="PropertyInfoHelper{T}"/> class
    /// </summary>
    [TestFixture]
    public class PropertyInfoHelperTests
    {
        [Test]
        public void TestPropertyInfo()
        {
            var one = PropertyInfoHelper<Bean>.GetPropertyInfo(b => b.Shoots);
            var two = typeof(Bean).GetProperty("Shoots");
            Assert.AreEqual(one, two);
        }

        [Test]
        public void TestINofityPropertyChanged()
        {
            string propName = null;
            var bean = new Bean();
            bean.PropertyChanged += (sender, args) => propName = args.PropertyName;
            bean.Shoots = 4;
            Assert.AreEqual(propName, "Shoots");
        }
    }
}
