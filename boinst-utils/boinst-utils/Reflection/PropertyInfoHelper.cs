﻿namespace Boinst.Utils.Reflection
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;

    /// <summary>
    /// The PropertyHelper class allows us to get a PropertyInfo without using
    /// a string argument.
    /// </summary>
    /// <typeparam name="T">
    /// The type to get the property from.
    /// </typeparam>
    public static class PropertyInfoHelper<T>
    {
        /// <summary>
        /// Get a Property by passing in a lambda expression
        /// </summary>
        /// <typeparam name="TVal">The expression value type</typeparam>
        /// <param name="selector">The selector</param>
        /// <returns>The property referenced by the expression.</returns>
        public static PropertyInfo GetPropertyInfo<TVal>(Expression<Func<T, TVal>> selector)
        {
            Expression body = selector;
            if (body is LambdaExpression) body = ((LambdaExpression)body).Body;
            if (body.NodeType != ExpressionType.MemberAccess) throw new InvalidOperationException();
            return (PropertyInfo)((MemberExpression)body).Member;
        }
    }
}
