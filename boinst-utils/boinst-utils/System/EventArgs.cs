﻿// ReSharper disable CheckNamespace
namespace System
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// A generic 'EventArgs' class with one argument
    /// </summary>
    /// <typeparam name="TFirst">The first argument</typeparam>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Multiple generic classes by the same name are appropriately collocated in this file.")]
    public class EventArgs<TFirst> : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventArgs{TFirst}"/> class.
        /// </summary>
        /// <param name="argument">
        /// The argument.
        /// </param>
        public EventArgs(TFirst argument)
        {
            this.First = argument;
        }

        /// <summary>
        /// The argument that was provided to this EventArgs instance.
        /// </summary>
        public TFirst First { get; set; }

        /// <summary>
        /// Implicitly conversion for an instance of type <see cref="TFirst"/>
        /// to a <see cref="EventArgs{TFirst}"/>.
        /// </summary>
        /// <param name="first">
        /// The first argument.
        /// </param>
        /// <returns>
        /// </returns>
        public static implicit operator EventArgs<TFirst>(TFirst first)
        {
            return new EventArgs<TFirst>(first);
        }
    }

    /// <summary>
    /// A generic 'EventArgs' class with two arguments
    /// </summary>
    /// <typeparam name="TFirst">The first argument</typeparam>
    /// <typeparam name="TSecond">The second argument</typeparam>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Multiple generic classes by the same name are appropriately collocated in this file.")]
    public class EventArgs<TFirst, TSecond> : EventArgs<TFirst>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventArgs{TFirst,TSecond}"/> class.
        /// </summary>
        /// <param name="first">
        /// The first argument.
        /// </param>
        /// <param name="second">
        /// The second argument.
        /// </param>
        public EventArgs(TFirst first, TSecond second)
            : base(first)
        {
            this.Second = second;
        }

        /// <summary>
        /// The second argument
        /// </summary>
        public TSecond Second { get; set; }
    }

    /// <summary>
    /// A generic 'EventArgs' class with three arguments
    /// </summary>
    /// <typeparam name="TFirst">The first argument</typeparam>
    /// <typeparam name="TSecond">The second argument</typeparam>
    /// <typeparam name="TThird">The third argument</typeparam>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Multiple generic classes by the same name are appropriately collocated in this file.")]
    public class EventArgs<TFirst, TSecond, TThird> : EventArgs<TFirst, TSecond>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventArgs{TFirst,TSecond,TThird}"/> class.
        /// </summary>
        /// <param name="first">
        /// The first argument.
        /// </param>
        /// <param name="second">
        /// The second argument.
        /// </param>
        /// <param name="third">
        /// The third argument.
        /// </param>
        public EventArgs(TFirst first, TSecond second, TThird third)
            : base(first, second)
        {
            this.Third = third;
        }

        /// <summary>
        /// The third argument
        /// </summary>
        public TThird Third { get; set; }
    }
}