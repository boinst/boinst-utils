﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("boinst-utils")]
[assembly: AssemblyDescription("This assembly contains generally useful utility classes.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Boinst")]
[assembly: AssemblyProduct("Boinst.Utils")]
[assembly: AssemblyCopyright("Copyright ©  2012 Boinst")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("c99291bd-ac9a-4299-8796-7eba0a18bc76")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]


