namespace Boinst.Utils.Reporting
{
    using global::System;

    /// <summary>
    /// A phase for use with the PhasedTask
    /// </summary>
    public sealed class MultipleIterationPhase : ProgressPhase
    {
        /// <summary>
        /// The number of iterations in this phase.
        /// </summary>
        private readonly int iterations;

        private int iteration = 1;

        private double iterationProgress;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultipleIterationPhase"/> class. 
        /// </summary>
        /// <param name="iterations">
        /// The total number of iterations in this phase.
        /// </param>
        public MultipleIterationPhase(int iterations)
        {
            this.iterations = iterations;
        }

        /// <summary>
        /// The progress of this phase, on a range of 0.0 to 1.0.
        /// </summary>
        public override double Progress
        {
            get
            {
                return base.Progress;
            }
            
            set
            {
                throw new Exception("Cannot set Progress directly on a MultipleIterationPhase. Instead call UpdateProgress.");
            }
        }

        /// <summary>
        /// Update the progress of this MultipleIterationPhase
        /// </summary>
        /// <param name="iteration">The number of the current iteration (starting at 1)</param>
        /// <param name="iterationProgress">The progress in this iteration</param>
        public void UpdateProgress(int iteration, double iterationProgress)
        {
            if (iteration < 1 || iteration > this.iterations) throw new ArgumentOutOfRangeException("iteration", iteration, "The value provided for \"iteration\" must be greater than or equal to 1, and no larger than the maximum number of iterations.");
            if (iterationProgress < 0.0 - 1e-6 || iterationProgress > 1.0 + 1e-6) throw new ArgumentOutOfRangeException("iterationProgress", iteration, "The value provided for \"iterationProgress\" must be on the range [0.0, 1.0].");
            iterationProgress = ProgressMath.Box(iterationProgress);
            this.iteration = iteration;
            this.iterationProgress = iterationProgress;
            base.Progress = (iteration - 1 + iterationProgress) / this.iterations;
        }

        /// <summary>
        /// Report on the progress of this phase.
        /// </summary>
        /// <returns></returns>
        public override string ReportProgress()
        {
            return string.Format("{0:0.0}% complete - iteration {1}/{2} {3:0.0}% complete.", this.Progress * 100, this.iteration, this.iterations, this.iterationProgress);
        }
    }
}