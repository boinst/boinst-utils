namespace Boinst.Utils.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// A utility class for calculating the overall progress of a phased task,
    /// where each task reports it's own progress.
    /// </summary>
    /// <remarks>
    /// Keeps an internal list of <see cref="IProgressPhase" /> instances.
    /// </remarks>
    public class PhasedTask
    {
        #region Fields

        /// <summary>
        /// The phases of this task
        /// </summary>
        private readonly List<IProgressPhase> phases = new List<IProgressPhase>();

        /// <summary>
        /// The time the task was started.
        /// </summary>
        private readonly DateTime startTime;

        /// <summary>
        /// The current phase. The most recent phase for which progress was reported.
        /// </summary>
        private IProgressPhase currentPhase;

        /// <summary>
        /// The progress of the overall Task
        /// </summary>
        private double progress;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PhasedTask"/> class.
        /// </summary>
        public PhasedTask()
        {
            this.startTime = DateTime.Now;
        }

        #endregion

        #region Public Events

        /// <summary>
        /// The progress changed.
        /// </summary>
        public event EventHandler<EventArgs> ProgressChanged;
        
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets InitialEstimate.
        /// </summary>
        public TimeSpan InitialEstimate { get; set; }

        /// <summary>
        /// Gets Progress.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        /// When the Progress is not in the range [0.0, 1.0]
        /// </exception>
        public double Progress
        {
            get
            {
                return this.progress;
            }

            private set
            {
                Debug.Assert(value >= this.progress, "We went backwards!");
                if (value < 0.0 - 1e-6 || value > 1.0 + 1e-6) 
                    throw new ArgumentOutOfRangeException("value", value, "The value provided for \"Progress\" must be on the range [0.0, 1.0].");

                value = ProgressMath.Box(value);
                this.progress = value;
                this.OnProgressChanged(value);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Add a phase to this task.
        /// </summary>
        /// <param name="phase">
        /// The phase to add
        /// </param>
        public void Add(IProgressPhase phase)
        {
            this.phases.Add(phase);
            phase.ProgressChanged += this.HandlePhaseProgressChanged;
            if (string.IsNullOrWhiteSpace(phase.Name))
            {
                phase.Name = "Phase " + this.phases.Count;
            }
        }

        /// <summary>
        /// Get a string reporting on the progress of the task.
        /// </summary>
        /// <returns>
        /// A message reporting on the progress of the task.
        /// </returns>
        public string GetProgressMessage()
        {
            var sb = new StringBuilder();
            sb.AppendFormat(
                "{0:0.0}% complete. ({1}: {2})", 
                this.progress * 100.0, 
                this.currentPhase.Name, 
                this.currentPhase.ReportProgress());

            TimeSpan elapsedTime = DateTime.Now - this.startTime;

            // Estimate remaining time unless we've reached 100% completion
            if (this.progress < 1.0 - 1e-6)
            {
                // if elapsed time and progress are both sensible figures, we can use them for our estimate.
                // We need progress above 2%,
                // And elapsed time is greater than one second, or greater than 2% of the initial estimate.
                // If these conditions are not met, then the InitialEstimate will be a better predictor of the completion time.
                if (this.progress >= 0.02
                    && (elapsedTime > TimeSpan.FromSeconds(1)
                     || (this.InitialEstimate != TimeSpan.Zero && elapsedTime.TotalSeconds > this.InitialEstimate.TotalSeconds * 0.02)))
                {
                    TimeSpan remaining = TimeSpan.FromSeconds(elapsedTime.TotalSeconds / this.Progress);
                    sb.AppendFormat(" Estimated {0} remaining.", FormatTime(remaining));
                }
                else if (this.InitialEstimate != TimeSpan.Zero && this.progress < 1.0 - 1e-6)
                {
                    TimeSpan remaining = this.InitialEstimate - elapsedTime;
                    sb.AppendFormat(" Estimated {0} remaining.", FormatTime(remaining));
                }
            }

            return sb.ToString();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prints a timespan as a nicely readable time.
        /// </summary>
        /// <remarks>
        /// 0 Milliseconds
        /// 999 Milliseconds
        /// 1 Seconds
        /// 89 Seconds
        /// 1.0 Minutes
        /// 99.9 Minutes
        /// </remarks>
        /// <param name="span">
        /// The timespan to format
        /// </param>
        /// <returns>
        /// A string representing the time
        /// </returns>
        private static string FormatTime(TimeSpan span)
        {
            if (span < TimeSpan.FromSeconds(1)) return string.Format("{0:0} Milliseconds", span.TotalMilliseconds);
            if (span < TimeSpan.FromMinutes(1)) return string.Format("{0:0} Seconds", span.TotalSeconds);
            return string.Format("{0:0.0} Minutes", span.TotalMinutes);
        }

        /// <summary>
        /// Called when the Progress of a Phase is changed.
        /// </summary>
        /// <param name="sender">
        /// The event sender
        /// </param>
        /// <param name="e">
        /// Event arguments
        /// </param>
        private void HandlePhaseProgressChanged(object sender, EventArgs<double> e)
        {
            this.currentPhase = (IProgressPhase)sender;

            double totalWeight = this.phases.Sum(progressPhase => progressPhase.Weight);
            double weightedProgressComplete = 0;

            foreach (IProgressPhase progressPhase in this.phases)
            {
                if (progressPhase == this.currentPhase)
                {
                    weightedProgressComplete += progressPhase.Progress * progressPhase.Weight;
                    break;
                }

                // any tasks that we've passed are presumed to have been completed
                weightedProgressComplete += progressPhase.Weight;
            }

            this.Progress = weightedProgressComplete / totalWeight;
        }
        
        /// <summary>
        /// Triggers the <see cref="ProgressChanged"/> event.
        /// </summary>
        /// <param name="value">
        /// The current progress.
        /// </param>
        private void OnProgressChanged(double value)
        {
            EventHandler<EventArgs> handler = this.ProgressChanged;
            if (handler != null) handler(this, new EventArgs<double>(value));
        }

        #endregion
    }
}