namespace Boinst.Utils.Reporting
{
    using System;

    /// <summary>
    /// A Phase used by the <see cref="PhasedTask"/>.
    /// </summary>
    public class ProgressPhase : IProgressPhase
    {
        /// <summary>
        /// The progress of this phase, on a range of 0.0 to 1.0.
        /// </summary>
        private double progress;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressPhase"/> class.
        /// </summary>
        public ProgressPhase()
            : this(1.0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressPhase"/> class.
        /// </summary>
        /// <param name="weight">
        /// The weight.
        /// </param>
        public ProgressPhase(double weight)
        {
            this.Weight = weight;
        }

        /// <summary>
        /// The progress of this Phase has changed.
        /// </summary>
        public event EventHandler<System.EventArgs<double>> ProgressChanged;
        
        /// <summary>
        /// The relative weight of this phase. Returns a value other than 1.0
        /// if this phase takes more or less time than other phases.
        /// </summary>
        public double Weight { get; private set; }

        /// <summary>
        /// The name of this phase.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The progress of this phase, on a range of 0.0 to 1.0.
        /// </summary>
        public virtual double Progress
        {
            get
            {
                return this.progress;
            }

            set
            {
                if (value < 0.0 - 1e-6 || value > 1.0 + 1e-6) throw new ArgumentOutOfRangeException("value", value, "The value provided for \"Progress\" must be on the range [0.0, 1.0].");
                value = ProgressMath.Box(value);
                if (this.progress.Equals(value)) return;
                
                this.progress = value;

                var handler = this.ProgressChanged;
                if (handler != null) handler(this, new EventArgs<double>(value));
            }
        }

        /// <summary>
        /// Report on the progress of this phase
        /// </summary>
        /// <returns>
        /// A text summary of the progress of this phase.
        /// </returns>
        public virtual string ReportProgress()
        {
            return string.Format("{0:0.0}% complete.", this.progress * 100);
        }
    }
}