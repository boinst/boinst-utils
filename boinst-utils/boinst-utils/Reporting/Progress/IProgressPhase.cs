namespace Boinst.Utils.Reporting
{
    using global::System;

    /// <summary>
    /// A Phase used by the <see cref="PhasedTask"/>.
    /// </summary>
    public interface IProgressPhase
    {
        /// <summary>
        /// The progress of this Phase has changed.
        /// </summary>
        event EventHandler<System.EventArgs<double>> ProgressChanged;
        
        /// <summary>
        /// The relative weight of this phase. Returns a value other than 1.0
        /// if this phase takes more or less time than other phases.
        /// </summary>
        double Weight { get; }

        /// <summary>
        /// The progress of this phase, on a range of 0.0 to 1.0.
        /// </summary>
        double Progress { get; }

        /// <summary>
        /// The name of this phase.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Return a string describing the progress of this phase.
        /// </summary>
        /// <returns>
        /// A text summary of the progress of this phase.
        /// </returns>
        string ReportProgress();
    }
}