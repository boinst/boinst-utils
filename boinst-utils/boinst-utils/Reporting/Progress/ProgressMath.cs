namespace Boinst.Utils.Reporting
{
    /// <summary>
    /// Utility methods used by the <see cref="PhasedTask"/> class
    /// and associated Phase classes.
    /// </summary>
    public static class ProgressMath
    {
        public static double Box(double value)
        {
            if (value < 0.0) return 0.0;
            if (value > 1.0) return 1.0;
            return value;
        }
    }
}